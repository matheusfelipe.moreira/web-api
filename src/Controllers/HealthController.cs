﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace web_app.Controllers
{
    public class HealthController : ApiController
    {
        [HttpGet]
        [ActionName("read")]
        public HttpResponseMessage GetRead()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Ok - read check");
        }

        [HttpGet]
        [ActionName("live")]
        public HttpResponseMessage GetLive()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Ok - live check");
        }

    }
}