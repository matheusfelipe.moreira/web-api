﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace web_app
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Serviços e configuração da API da Web

            // Rotas da API da Web
            config.MapHttpAttributeRoutes();
            /*
            config.Routes.MapHttpRoute(
                name: "Health",
                routeTemplate: "v1/{controller}/{action}",
                defaults: new { action = "GetRead"}
            );*/

            config.Routes.MapHttpRoute(
                name: "HealthLive",
                routeTemplate: "v1/health/live",
                defaults: new { controller = "Health", action = "live" }
            );

            config.Routes.MapHttpRoute(
                "HealthRead",
                "v1/health/read",
                new { controller = "Health", action = "read" }
            );

            config.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "v1/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


        }
    }
}
